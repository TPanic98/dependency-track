import argparse
import sys
import requests
from reportlab.pdfbase import pdfmetrics   # 注册字体
from reportlab.pdfbase.ttfonts import TTFont # 字体类
from reportlab.platypus import Table, SimpleDocTemplate, Paragraph, Image  # 报告内容相关类
from reportlab.lib.pagesizes import letter  # 页面的标志尺寸(8.5*inch, 11*inch)
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.styles import getSampleStyleSheet  # 文本样式
from reportlab.lib import colors  # 颜色模块
from reportlab.graphics.charts.barcharts import VerticalBarChart  # 图表类
from reportlab.graphics.charts.legends import Legend  # 图例类
from reportlab.graphics.shapes import Drawing  # 绘图工具
from reportlab.lib.units import cm  # 单位：cm
from reportlab.pdfgen import canvas
import json
import os
from datetime import datetime
import PyPDF2
    #使用事例：python get_project.py -id 27affb6d-d42f-40e7-824d-5d68cd64443a

font_path = 'simsun.ttc'
# 注册字体(提前准备好字体文件, 如果同一个文件需要多种字体可以注册多个)
pdfmetrics.registerFont(TTFont('SimSun', font_path))

# 页眉页脚设置
class FooterCanvas(canvas.Canvas):

    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)
        for page in self.pages:
            self.__dict__.update(page)
            self.draw_canvas(page_count)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_canvas(self, page_count):
        page = " www.hkbea.com.cn                        第%s页 共%s页" % (self._pageNumber, page_count)
        x = 128
        self.saveState()
        self.setStrokeColorRGB(0, 0, 0)
        self.setLineWidth(0.5)
        self.line(66, 78, LETTER[0] - 66, 78)
        self.line(66, letter[1]-5, LETTER[0] - 66, letter[1]-5)
        self.setFont('SimSun', 15)
        self.setFillColor(aColor=colors.red)
        self.drawString(80, letter[1], "东亚银行（中国）有限公司                        BEA·QingZhui")
        self.setFont('SimSun', 10)
        self.setFillColor(aColor=colors.grey)
        self.drawString(80, 65, page)
        self.restoreState()

# def pic(canvas,doc)：
#     canvas

class Graphs:
    # 绘制开局标题
    @staticmethod
    def draw_big_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 50            # 字体大小
        ct.leading = 50             # 行间距
        ct.alignment = 0   # 居中
        ct.bold = True
        ct.spaceBefore = 200
        ct.spaceAfter = 500

        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    
    # 绘制大横杆
    @staticmethod
    def draw_longbar(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 1            # 字体大小
        ct.leading = 2             # 行间距
        ct.alignment = 0            # 不居中
        ct.bold = True
        ct.backColor = colors.silver
        ct.spaceBefore = 13
        ct.spaceAfter = 1
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    # 绘制小横杆
    @staticmethod
    def draw_smallbar(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 1            # 字体大小
        ct.leading = 1             # 行间距
        ct.alignment = 0            # 不居中
        ct.bold = True
        ct.backColor = colors.lightgrey
        ct.spaceBefore = 0
        ct.spaceAfter = 0
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    
    # 绘制标题
    @staticmethod
    def draw_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 20            # 字体大小
        ct.leading = 10             # 行间距
        ct.textColor = colors.black     # 字体颜色
        ct.alignment = 0    # 居中
        ct.bold = True
        ct.spaceBefore = 10
        ct.spaceAfter = 0
        # ct.backColor = colors.green
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
      
  # 绘制小标题
    @staticmethod
    def draw_little_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Normal']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'  # 字体名
        ct.fontSize = 15  # 字体大小
        ct.leading = 15  # 行间距
        ct.textColor = colors.black  # 字体颜色
        ct.spaceBefore = 10
        ct.spaceAfter = 5
        # ct.backColor = colors.green
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    

    # 绘制普通段落内容
    @staticmethod
    def draw_text(text: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 获取普通样式
        ct = style['Normal']
        ct.fontName = 'SimSun'
        ct.fontSize = 12
        ct.wordWrap = 'CJK'     # 设置自动换行
        ct.alignment = 0        # 左对齐
        ct.firstLineIndent = 32     # 第一行开头空格
        ct.leading = 25
        return Paragraph(text, ct)

    # 绘制表格
    @staticmethod
    def draw_table(*args):
        # 1列宽度
        # col_width = 200
        style = [
            ('FONTNAME', (0, 0), (-1, -1), 'SimSun'),  # 字体
            ('FONTSIZE', (0, 0), (-1, 0), 13),  # 第一行的字体大小
            ('FONTSIZE', (0, 1), (-1, -1), 10),  # 第二行到最后一行的字体大小
            ('BACKGROUND', (0, 0), (-1, 0), '#d5dae6'),  # 设置第一行背景颜色
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # 第一行水平居中
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),  # 第二行到最后一行左右左对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 所有表格上下居中对齐
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),  # 设置表格内文字颜色
            ('GRID', (0, 0), (-1, -1), 0.5, colors.grey),  # 设置表格框线为grey色，线宽为0.5
            ('SPAN', (0, 0), (1, 0)),  # 合并第一列二三行

        ]
        table = Table(args, colWidths=[100,200], style=style,hAlign='LEFT')
        return table
    # 绘制表格
    @staticmethod
    def draw_table2(*args):
        # 1列宽度
        # col_width = 200
        
        style = [
            ('FONTNAME', (0, 0), (-1, -1), 'SimSun'),  # 字体
            ('FONTSIZE', (0, 0), (-1, 0), 13),  # 第一行的字体大小
            ('FONTSIZE', (0, 1), (-1, -1), 10),  # 第二行到最后一行的字体大小
            ('BACKGROUND', (0, 0), (-1, 0), '#d5dae6'),  # 设置第一行背景颜色
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # 第一行水平居中
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),  # 第二行到最后一行左右左对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 所有表格上下居中对齐
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),  # 设置表格内文字颜色
            ('GRID', (0, 0), (-1, -1), 0.5, colors.grey),  # 设置表格框线为grey色，线宽为0.5
            ('SPAN', (0, 0), (-1, 0)),  # 合并第一列二三行            
        ]
        table = Table(args, colWidths=[100,60,150,100,60], style=style,hAlign='LEFT',)
        return table
    # 绘制表格
    @staticmethod
    def draw_table3(*args):
        # 1列宽度
        # col_width = 200
        
        style = [
            ('FONTNAME', (0, 0), (-1, -1), 'SimSun'),  # 字体
            ('FONTSIZE', (0, 0), (-1, 0), 13),  # 第一行的字体大小
            ('FONTSIZE', (0, 1), (-1, -1), 10),  # 第二行到最后一行的字体大小
            ('BACKGROUND', (0, 0), (-1, 0), '#d5dae6'),  # 设置第一行背景颜色
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # 第一行水平居中
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),  # 第二行到最后一行左右左对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 所有表格上下居中对齐
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),  # 设置表格内文字颜色
            ('GRID', (0, 0), (-1, -1), 0.5, colors.grey),  # 设置表格框线为grey色，线宽为0.5
            ('SPAN', (0, 0), (-1, 0)),  # 合并第一列二三行            
        ]
        table = Table(args, colWidths=[150,120,80,40,40,40], style=style,hAlign='LEFT',)
        return table
    # 创建图表
    @staticmethod
    def draw_bar(bar_data: list, ax: list, items: list):
        drawing = Drawing(500, 250)
        bc = VerticalBarChart()
        bc.x = 45       # 整个图表的x坐标
        bc.y = 45      # 整个图表的y坐标
        bc.height = 200     # 图表的高度
        bc.width = 350      # 图表的宽度
        bc.data = bar_data
        bc.strokeColor = colors.black       # 顶部和右边轴线的颜色
        bc.valueAxis.valueMin = 5000           # 设置y坐标的最小值
        bc.valueAxis.valueMax = 26000         # 设置y坐标的最大值
        bc.valueAxis.valueStep = 2000         # 设置y坐标的步长
        bc.categoryAxis.labels.dx = 2
        bc.categoryAxis.labels.dy = -8
        bc.categoryAxis.labels.angle = 20
        bc.categoryAxis.categoryNames = ax

        # 图示
        leg = Legend()
        leg.fontName = 'SimSun'
        leg.alignment = 'right'
        leg.boxAnchor = 'ne'
        leg.x = 475         # 图例的x坐标
        leg.y = 240
        leg.dxTextSpace = 10
        leg.columnMaximum = 3
        leg.colorNamePairs = items
        drawing.add(leg)
        drawing.add(bc)
        return drawing

    # 绘制图片
    @staticmethod
    def draw_img(path):
        img = Image(path,hAlign='RIGHT')       # 读取指定路径下的图片


        img.drawWidth = 22*cm        # 设置图片的宽度
        img.drawHeight = 21*cm       # 设置图片的高度
        
        return img
    


if __name__ == '__main__':
    
    # 接受信息 事例：python get_project.py -id 5bd452bf-fdf2-475d-bfc9-18f541b0574c 
    ##-u http://127.0.0.1:8081 -k lWMRDCX5rQ3oEQ6IZMrUX6xIdOJUAed3 
    parser = argparse.ArgumentParser(prog = 'dt_program_query',description = 'query program')
    parser.add_argument('-id', '--uuid', help='The UUID of the project to retrieve') 
    # parser.add_argument('-u', '--url', help='Dependency Track base URL like http://localhost:8080')
    # parser.add_argument('-k', '--key', help='Dependency Track API Key')
    

    args = parser.parse_args()

    if (args.uuid is None ):
        parser.print_help()
        sys.exit(1)

    uuid = args.uuid
    key = 'lWMRDCX5rQ3oEQ6IZMrUX6xIdOJUAed3'
    api_key_header = {'X-Api-Key': key}

    aurl = 'http://127.0.0.1:8081'
    r = requests.get(url=aurl+'/api/v1/vulnerability/project/'+uuid, headers=api_key_header)
    print ('vul read statement:',r)
    json_dict = json.loads(r.text)
    # print (type(r.text))
    # filename = uuid+'.json'
    # fo = open(filename, "w",encoding= 'utf-8')
    # fo.write( r.text)   


    r = requests.get(url=aurl+'/api/v1/component/project/'+uuid, headers=api_key_header)
    print ('project read statement:',r)
    # filename2 = uuid+'2.json'
    # fo2 = open(filename2, "w",encoding= 'utf-8')
    # fo2.write( r.text)  
    json_dict2 = json.loads(r.text)

    # 获得漏洞数量
    r = requests.get(url=aurl+'/api/v1/metrics/project/'+uuid+'/current', headers=api_key_header)
    json_dict3 = json.loads(r.text)

    # 获得违反项目信息
    r = requests.get(url=aurl+'/api/v1/violation/project/'+uuid, headers=api_key_header)
    json_dict4 = json.loads(r.text)
    # print(json_dict4)

    # 创建内容对应的空列表
    content = list()
    content2 = list()
    style1 = getSampleStyleSheet()
    style2 = style1['Normal']
    style2.fontSize = 1

    # content.append(Graphs.draw_img('D:\\vscodews\\python\\bea2.png'))
    # content.append(Graphs.draw_img('D:\\vscodews\\python\\bea2.png'))
    content2.append(Image('D:\\vscodews\\python\\bea2.png',width=21*cm,height=27*cm,hAlign='LEFT',))




    # content.append(Paragraph('1',style=style2))
    # content.append(Graphs.draw_big_title('青追漏洞管理平台\n漏洞报告'))
    

    # fo = open(filename, "r",encoding='utf-8')
    # fo2 = open(filename2, "r",encoding='utf-8')
    # json_dict = json.load(fo)
    # json_dict2 = json.load(fo2)

    project = json_dict2[0]['project']
    # 添加标题e
    content.append(Graphs.draw_title(str('项目'+project['name']+'的扫描结果')))
    content.append(Graphs.draw_longbar('1'))


    # 添加小标题
    content.append(Graphs.draw_little_title('项目信息'))
    content.append(Graphs.draw_smallbar('1'))
    content.append(Graphs.draw_title(''))
    # 添加表格
    data = [
        ('项目信息',),
        ('项目名称', project['name']),
        ('项目id',project['uuid']),
        ('扫描时间', datetime.fromtimestamp(int(project['lastBomImport']/1000))),
        ('威胁评分',project['lastInheritedRiskScore'])

    ]
    content.append(Graphs.draw_table(*data))

    # 添加小标题
    content.append(Graphs.draw_little_title('漏洞信息'))
    content.append(Graphs.draw_smallbar('1'))
    content.append(Graphs.draw_title(''))


    data = [
        ('漏洞统计',),
        ('严重', json_dict3['critical']),
        ('高危',json_dict3['high']),
        ('中危',json_dict3['medium']),
        ('低危险',json_dict3['low'])
    ]
    content.append(Graphs.draw_table(*data))

    content.append(Graphs.draw_little_title('详细信息:'))
    content.append(Graphs.draw_smallbar('1'))
    content.append(Graphs.draw_title(''))
    content.append(Graphs.draw_little_title('许可证详细信息'))

    data2 = [
        ('具体信息前往青追平台查看：',),
        ('组件名称', '许可证名称','许可证id','OSI批准','FSF许可','已弃用'),      
    ]
    ii = 1
    for id in json_dict2:
        if ii != 1:                  
            if (id['name'] == data2[ii][0]):                
                continue
        ii = ii + 1
        license_name = ''
        license_id = ''
        osi = ''
        fsf = ''
        DeprecatedLicense = ''
        # CustomLicense = ''
        if id.get('resolvedLicense') != None:
            license_name = id['resolvedLicense']['name']
            license_id = id['resolvedLicense']['licenseId']
            if id['resolvedLicense']['isOsiApproved'] == True:
                osi = '√'
            # osi = id['resolvedLicense']['isOsiApproved']
            if id['resolvedLicense']['isFsfLibre'] == True:
                fsf = '√'
            if id['resolvedLicense']['isDeprecatedLicenseId'] == True:
                DeprecatedLicense = '√'
            # if id['resolvedLicense']['isCustomLicense'] == True:
            #     CustomLicense = '√'
            # fsf = id['resolvedLicense']['isFsfLibre']
            # DeprecatedLicense = id['resolvedLicense']['isDeprecatedLicenseId']
            # CustomLicense = id['resolvedLicense']['isCustomLicense']
        else:
            ii = ii - 1
            continue
 
        data2.append((
                id['name'],
                Paragraph(license_name),
                Paragraph(license_id),
                osi,
                fsf,
                DeprecatedLicense,
                # CustomLicense                
        ))       
        
        


    content.append(Graphs.draw_table3(*data2))
    content.append(Graphs.draw_little_title('漏洞详细信息'))

    # 添加表格
    data = [
        ('具体信息前往青追平台查看：',),
        ('组件名称', '版本','组','弱点名称','严重程度'),       

    ]
    i = 0
    for id in json_dict:
        data.append((
                Paragraph(json_dict[i]['components'][0]['name']),
                Paragraph(json_dict[i]['components'][0]['version']),
                Paragraph(json_dict[i]['components'][0]['group']),
                Paragraph(json_dict[i]['vulnId']),
                Paragraph(json_dict[i]['severity'])
        ))    
        i = i + 1
    content.append(Graphs.draw_table2(*data))

    
    # content.append(Graphs.draw_table3(*data2))
    # content.append(Graphs.draw_little_title('策略违规信息'))

    # if len(json_dict4) != 0:
        
    #     data = [
    #     ('具体信息前往青追平台查看：',),
    #     ('违规组件', '违规版本','违规类型','违规名称','违规程度'),     
    #     ]
    #     for id in json_dict4:
            
    #         data.append((
    #             id['component']['name'],
    #             id['component']['version'],
    #             id['type'],
    #             id['policyCondition']['policy']['name'],
    #             id['policyCondition']['policy']['violationState']
    #             # CustomLicense                
    #         ))    


    #     content.append(Graphs.draw_table2(*data))



    # 生成pdf文件
    doc2 = SimpleDocTemplate('report2.pdf', pagesize=[21*cm,27.5*cm],leftMargin = 0,rightMargin = 0,topMargin = 0,bottomMargin = 0)
    doc2.build(content2)

    doc = SimpleDocTemplate('report1.pdf', pagesize=[21*cm,29.7*cm])
    doc.build(content,canvasmaker=FooterCanvas)

    filenames = ['report2.pdf','report1.pdf']
    merger = PyPDF2.PdfMerger()
    for file in filenames:
        merger.append(PyPDF2.PdfReader(file))
    merger.write('项目'+project['name']+'扫描结果.pdf')
    os.remove('report1.pdf')
    os.remove('report2.pdf')
    # fo.close
    # fo2.close
    # os.remove(filename)
    # os.remove(filename2)

    print('success! \n 打开'+project['name']+'扫描结果.pdf 查看结果')
    os.system('项目'+project['name']+'扫描结果.pdf')
    # os.system("report2.pdf")
