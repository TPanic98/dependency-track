/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.resources.v1;

import alpine.Config;
import alpine.common.logging.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.datanucleus.PropertyNames;
import org.datanucleus.api.jdo.JDOPersistenceManagerFactory;
import org.dependencytrack.cnvd.Cnvd;
import org.dependencytrack.cnvd.QueryCnvd;

import org.dependencytrack.parser.nvd.CnvdParser;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Path("/v1/cnvd")
@Api(value = " cnvd",authorizations = @Authorization(value = "X-Api-Key"))
public class CnvdResource {
    private final JDOPersistenceManagerFactory pmf = createPersistenceManagerFactory();
    private static final Logger LOGGER = Logger.getLogger(CnvdResource.class);

    //curl -X GET http://127.0.0.1:8080/api/v1/cnvd/CVE-2023-6530 -H X-Api-Key:odt_tU6uHr6dqSfEmeIarhdFm6Vkw6gVTQX1
    @GET
    @Path("/{cveId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getCnvd(@PathParam("cveId") String cveId){
        LOGGER.info("----------processing getCnvd api, the query cveid is :"+cveId);
        String cnvdstring = cvequery(cveId);
        if (cnvdstring!= null){
            return Response.ok(cnvdstring).build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    //curl -X GET "http://127.0.0.1:8080/api/v1/cnvd?sortName=published&sortOrder=desc&pageSize=10&pageNumber=1" -H X-Api-Key:odt_tU6uHr6dqSfEmeIarhdFm6Vkw6gVTQX1
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAll(@QueryParam("sortName") String sortName,
                         @QueryParam("sortOrder") String sortOrder,
                         @DefaultValue("10")@QueryParam("pageSize") Long pageSize,
                         @DefaultValue("1")@QueryParam("pageNumber") Long pageNumber
                         ){
        PersistenceManager pm = pmf.getPersistenceManager();
        LOGGER.info("processing query of sortname:"+sortName+" sortorder:"+sortOrder+" pagesize:"+pageSize+" pagenumber:"+pageNumber);
        try {
            Query query = pm.newQuery(Cnvd.class);
            if(sortName != null && sortOrder != null){
                query.setOrdering(sortName+" "+sortOrder);
            }
            query.setRange( (pageNumber - 1) *pageSize, pageNumber *pageSize);

            List<Cnvd> results = (List<Cnvd>) query.execute();
            if (results == null){
                return Response.status(Response.Status.NOT_FOUND).entity("Can not find any cnvd").build();
            }
            List<String> res = new ArrayList<>();

            for(Cnvd cnr : results){
                res.add(cnr.toString());
            }
            return Response.ok(String.join(" ",res)).build();
        }catch (Exception e){
            LOGGER.info("---------"+e.getMessage());
            return Response.status(Response.Status.NOT_FOUND).entity("Some error happened when processing").build();
        }
        finally {
            try {
                if(pm != null && !pm.isClosed()){
                    pm.close();
                }
            }catch (Exception e){
                LOGGER.info("++++++++++++"+e.getMessage());
            }
        }
    }

    //测试可修改前端文件frontend/src/shared/api.json "URL_BOM": "api/v1/cnvd/upload",
    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_XML)
    public Response CnvdApiUpload(final FormDataMultiPart multiPart){
        final List<FormDataBodyPart> artifactParts = multiPart.getFields("bom");
        for (final FormDataBodyPart artifactPart: artifactParts) {
            final BodyPartEntity bodyPartEntity = (BodyPartEntity) artifactPart.getEntity();
            try (InputStream in = bodyPartEntity.getInputStream()) {
                final String content = IOUtils.toString(new BOMInputStream((in)), StandardCharsets.UTF_8);
                Document doc = Jsoup.parse(content);
                CnvdParser.parseIt(doc);
            } catch (IOException e) {
                   LOGGER.info("++++++++++++++++++"+e.getMessage());
            }
        }
        return Response.ok().build();
    }

    private String cvequery(String cveId){
        Cnvd cnvd = QueryCnvd.queryCnvd("cveId",cveId);
        if (cnvd == null){return null;}
        else             {return cnvd.toString();}
    }

    private static JDOPersistenceManagerFactory createPersistenceManagerFactory() {
        final var dnProps = new Properties();
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_URL, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_URL));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_DRIVER_NAME, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_DRIVER));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_USER_NAME, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_USERNAME));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_PASSWORD, Config.getInstance().getPropertyOrFile(Config.AlpineKey.DATABASE_PASSWORD));
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_DATABASE, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_TABLES, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_COLUMNS, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_CONSTRAINTS, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_GENERATE_DATABASE_MODE, "create");
        dnProps.put(PropertyNames.PROPERTY_QUERY_JDOQL_ALLOWALL, "true");
        return (JDOPersistenceManagerFactory) JDOHelper.getPersistenceManagerFactory(dnProps, "Alpine");
    }
}
