/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.cnvd;

import org.json.JSONArray;
import org.json.JSONObject;

import alpine.common.logging.Logger;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CnvdUpdate {
    private static final Logger LOGGER = Logger.getLogger(CnvdUpdate.class);

    public static void update() {
        Calendar calendar = Calendar.getInstance();

        String starttime = QueryCnvd.queryUpdateTime();
//        String starttime = "2024-01-29";
        // 格式化日期为字符串
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String endtime = sdf.format(calendar.getTime());

        // 打印结果
        LOGGER.info("cnvdupdate  is updating from " +starttime+" to "+ endtime);
        int pageIndex = 1;
        downloadJson(pageIndex, starttime,endtime);
        LOGGER.info("cnvd update finished ");
    }

    private static void downloadJson(int pageIndex, String startDate, String endDate) {

        String response = CnvdHttp.cnvdRequest(pageIndex, startDate, endDate);

        JSONObject obj = new JSONObject(response);

        int total = obj.getJSONObject("data").getInt("total");
        pageIndex = obj.getJSONObject("data").getInt("pageIndex");
        int pageSize = obj.getJSONObject("data").getInt("pageSize");

        if (obj.getBoolean("success")) {
            //处理得到的结果
            processJson(obj);
            //如果不是最后一页进行递归
            int remaining = total + pageSize - pageIndex * pageSize;
            if (remaining > 0) {
                downloadJson(pageIndex + 1, startDate,endDate);
            }
        } else {
            LOGGER.info("failed to process " + total + "," + pageIndex + "," + pageSize);
        }

    }

    private static void processJson(JSONObject obj) {

        JSONArray records = obj.getJSONObject("data").getJSONArray("records");
        // System.out.println(records);
        int length = records.length();
        // 提取数据
        for (int i = 0; i < length; i++) {
            JSONObject arecord = records.getJSONObject(i);
            String id = arecord.getString("id");
            String cnnvdCode = arecord.getString("cnnvdCode");
            String vulType = arecord.getString("vulType");

            // 打印提取的数据

            String output = RequestDetail.getDetail(cnnvdCode, id, vulType);
            try {
                JSONObject detailobj1 = new JSONObject(output);
                JSONObject detailobj = detailobj1.getJSONObject("data").getJSONObject("cnnvdDetail");


                // 储存数据
                Cnvd cnnvd = new Cnvd();
                // 提取name节点的值
                cnnvd.setName(detailobj.getString("vulName"))  ;
                // 提取vuln-id节点的值
                cnnvd.setVulnId(detailobj.getString("cnnvdCode")) ;
                // 提取published节点的值
                String published = detailobj.getString("publishTime");
                cnnvd.setPublished(deleteTime(published));
                // 提取severity节点的值
                cnnvd.setSeverity(String.valueOf(detailobj.optInt("hazardLevel",0)));
                // 提取vuln-descript节点的值
                cnnvd.setVulnDescript(detailobj.getString("vulDesc"));
                // 提取cve-id节点的值
                cnnvd.setCveId(detailobj.getString("cveCode"));
                // 提取vuln-solution节点的值
                cnnvd.setvulnSolution(detailobj.optString("patch", ""));

                //提取updateTime节点的值，删除具体时间仅保留日期
                String modified = detailobj.getString("updateTime");
                cnnvd.setmodified(deleteTime(modified));
                // 提取vulTypeName节点的值
                cnnvd.setvulntype(detailobj.optString("vulTypeName",""));
                // 提取referUrl节点的值
                cnnvd.setsource(detailobj.getString("referUrl"));

                //上传cnnvd至数据库
                UploadCnvd.Upload(cnnvd);

            }catch (Exception e){
                LOGGER.info(e.getMessage());
            }
        }
    }



    private static String deleteTime(String dateStr) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // 解析字符串为 LocalDate 对象
        LocalDate date = LocalDate.parse(dateStr, inputFormatter);

        // 提取日期部分
        return date.toString();
    }
}
