/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.cnvd;

import alpine.common.logging.Logger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;

public class RequestDetail {
    private static final Logger LOGGER = Logger.getLogger(RequestDetail.class);
    

    public static String getDetail(String cnnvdCode,String id,String vulType) {
            try {

                // 替换为实际请求的URL
                String apiUrl = "https://www.cnnvd.org.cn/web/cnnvdVul/getCnnnvdDetailOnDatasource";
                // 替换为实际的请求体数据
                String requestBody = "{\"cnnvdCode\": \""+cnnvdCode+"\", \"id\": \""+id+"\",\"vulType\": \""+vulType+"\"}";

                // 发送POST请求
                String response = sendPostRequest(apiUrl, requestBody);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LOGGER.info(e.getMessage());
                }
                // 打印响应
                // System.out.println("Response: " + response);
                
                return response;
            } catch (IOException e) {
                LOGGER.info(e.getMessage());
                return "";
            }            
        }

    private static String sendPostRequest(String apiUrl, String requestBody) throws IOException {

        System.setProperty("proxyType","4");
        System.setProperty("proxyPort","8088");
        System.setProperty("proxyHost","10.192.201.210");
        System.setProperty("proxySet","true");

            URL url = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    
            // 设置请求方法为POST
            connection.setRequestMethod("POST");
    
            // 设置请求头
            connection.setRequestProperty("Accept", "application/json, text/plain, */*");
            connection.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
            connection.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
            connection.setRequestProperty("Connection", "keep-alive");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Host", "www.cnnvd.org.cn");
            connection.setRequestProperty("Origin", "https://www.cnnvd.org.cn");
            connection.setRequestProperty("Referer", "https://www.cnnvd.org.cn/home/loophole");
            connection.setRequestProperty("Sec-Fetch-Dest", "empty");
            connection.setRequestProperty("Sec-Fetch-Mode", "cors");
            connection.setRequestProperty("Sec-Fetch-Site", "same-origin");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36");
    
            // 设置其他请求头
            connection.setRequestProperty("Access-Control-Allow-Credentials", "true");
            connection.setRequestProperty("Access-Control-Allow-Origin", "http://172.16.101.43");
            connection.setRequestProperty("Access-Control-Expose-Headers", "token");
            connection.setRequestProperty("Vary", "Access-Control-Request-Headers");
            connection.setRequestProperty("Vary", "Access-Control-Request-Method");
            connection.setRequestProperty("Vary", "Origin");
    
            // 设置请求体
            connection.setDoOutput(true);
            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = requestBody.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
    
            // 获取响应
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return response.toString();
            }
        }
}
