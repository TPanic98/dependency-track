/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.cnvd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.IdentityType;


/**
 * Model for tracking vulnerabilities.
 *
 * @author Steve Springett
 * @since 3.0.0
 */


@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Cnvd {
    
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private long id;
    @Persistent
    @Column(name = "cname", allowsNull = "false")
    private String cname;
    @PrimaryKey
    @Persistent
    @Column(name = "vulnId", allowsNull = "false")
//    @Column(name = "vulnId")
    private String vulnId;
    @Persistent
    @Column(name = "published", allowsNull = "false")
//    @Column(name = "published")
    private String published;
    @Persistent
    @Column(name = "severity")
    private String severity;
    @Persistent
    @Column(name = "vulnDescript",length = 1000)
    private String vulnDescript;
    @Persistent
    @Column(name = "cveId", allowsNull = "false")
    private String cveId;
    @Persistent
    @Column(name = "vulnSolution",length = 1000)
    private String vulnSolution;
    @Persistent
    @Column(name = "modified")
    private String modified;
    @Persistent
    @Column(name = "vulntype")
    private String vulntype;
    @Persistent
    @Column(name = "source",length = 1000)
    private String source;


    //重写输出函数，直接输出对象的json格式
    @Override
    public String toString(){
        try {
            ObjectMapper objctmapper = new ObjectMapper();
            return objctmapper.writeValueAsString(this);
        }catch (JsonProcessingException e){
            e.printStackTrace();
            return super.toString();
        }
    }

    public Long getId(){
        return id;
    }
    public void setName(String name) {
        this.cname = name;
    }
    public String getName(){
        return cname;
    }
    public void setVulnId(String vulnId) {
        this.vulnId = vulnId;
    }
    public String getVulnId(){
        return vulnId;
    }
    public void setPublished(String published) {
        this.published = published;
    }
    public String getPublished(){
        return published;
    }
    public void setSeverity(String severity) {
        switch (severity) {
            case "1" -> this.severity = "超危";
            case "2" -> this.severity = "高危";
            case "3" -> this.severity = "中危";
            case "4" -> this.severity = "低危";
            case "0" -> this.severity = "未定义";
            default -> this.severity = severity;
        }
    }
    public String getSeverity(){
        return severity;
    }
    public void setVulnDescript(String vulnDescript) {
        if (vulnDescript.length() > 999){
            this.vulnDescript = vulnDescript.substring(0,999);
        }else {
            this.vulnDescript = vulnDescript;
        }
    }
    public String getVulnDescript(){
        return vulnDescript;
    }
    public void setCveId(String cveId) {
        this.cveId = cveId;
    }
    public String getCveId(){
        return cveId;
    }
    public void setvulnSolution(String vulnSolution) {
        if (vulnSolution.length() > 999){
            this.vulnSolution = vulnSolution.substring(0,999);
        }else {
            this.vulnSolution = vulnSolution;
        }
    }
    public String getvulnSolution(){
        return vulnSolution;
    }
    public void setmodified(String modified) {
        this.modified = modified;
    }
    public String getmodified(){
        return modified;
    }
    public void setvulntype(String vulntype) {
        this.vulntype = vulntype;
    }
    public String getvulntype(){
        return vulntype;
    }
    public void setsource(String source) {
        if (source.length() > 999){
            this.source = source.substring(0,999);
        }else {
            this.source = source;
        }
    }
    public String getsource(){
        return source;
    }

}




