/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.cnvd;

import alpine.Config;
import alpine.common.logging.Logger;
import org.datanucleus.PropertyNames;
import org.datanucleus.api.jdo.JDOPersistenceManagerFactory;
//import org.datanucleus.identity.ObjectId;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import java.util.Properties;
public class UploadCnvd {
    private static final Logger LOGGER = Logger.getLogger(UploadCnvd.class);


    public static void Upload(Cnvd cnnvd){
        final JDOPersistenceManagerFactory pmf = createPersistenceManagerFactory();
        final PersistenceManager pm = pmf.getPersistenceManager();

        try{

            Query query = pm.newQuery(Cnvd.class);
            query.setFilter("vulnId == '"+cnnvd.getVulnId()+"'");
            query.setUnique(true);
            Cnvd cnr = (Cnvd) query.execute();

            //重复词条会被覆盖，若希望重复词条直接跳过直接注释else部分
            if (cnr == null){
                pm.makePersistent(cnnvd);
            }else {
                pm.deletePersistent(cnr);
                pm.makePersistent(cnnvd);
            }

        }catch (Exception e){
            LOGGER.info("++++++++++++"+e.getMessage());

        }
        finally {
                try {
                    if(pm != null && !pm.isClosed()){
                        pm.close();
                    }
                    if(pmf != null && !pmf.isClosed()){
                        pmf.close();
                    }
                }catch (Exception e){
                    LOGGER.info("++++++++++++"+e.getMessage());
                }
        }
    }
    private static JDOPersistenceManagerFactory createPersistenceManagerFactory() {
        final var dnProps = new Properties();
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_URL, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_URL));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_DRIVER_NAME, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_DRIVER));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_USER_NAME, Config.getInstance().getProperty(Config.AlpineKey.DATABASE_USERNAME));
        dnProps.put(PropertyNames.PROPERTY_CONNECTION_PASSWORD, Config.getInstance().getPropertyOrFile(Config.AlpineKey.DATABASE_PASSWORD));
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_DATABASE, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_TABLES, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_COLUMNS, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_AUTOCREATE_CONSTRAINTS, "true");
        dnProps.put(PropertyNames.PROPERTY_SCHEMA_GENERATE_DATABASE_MODE, "create");
        dnProps.put(PropertyNames.PROPERTY_QUERY_JDOQL_ALLOWALL, "true");
        return (JDOPersistenceManagerFactory) JDOHelper.getPersistenceManagerFactory(dnProps, "Alpine");
    }
}
