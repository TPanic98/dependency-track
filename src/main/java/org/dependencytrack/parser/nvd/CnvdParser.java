/*
 * This file is part of Dependency-Track.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) Steve Springett. All Rights Reserved.
 */
package org.dependencytrack.parser.nvd;

import alpine.common.logging.Logger;

import org.dependencytrack.cnvd.Cnvd;
//import org.dependencytrack.cnvd.QueryCnvd;
import org.dependencytrack.cnvd.UploadCnvd;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;


/**
 * Parser and processor of CnVD data feeds.
 */

// src\main\java\org\dependencytrack\parser\nvd\CnvdParser进行解析，存储到数据库dtrack中的CNVD表
//
//fileName = "D:\\vscodews\\dependency-track-master\\dependency-track-master\\2023年.xml"
public final class CnvdParser {

    private static final Logger LOGGER = Logger.getLogger(CnvdParser.class);

    public static void parse(String fileName) {
        LOGGER.info("cnvdpaser start" );

        try {
            File xmlFile = new File(fileName);
            Document doc = Jsoup.parse(xmlFile, "UTF-8", "", org.jsoup.parser.Parser.xmlParser());
            LOGGER.info("reading the file :" + fileName);
            parseIt(doc);
        } catch (Exception e) {
            LOGGER.info("unable to load file:", e);
        }
    }

    public static void parseIt(Document doc){
        Elements entries = doc.select("entry");

        try {
            for (Element entry : entries) {
            Cnvd cnnvd = new Cnvd();
            cnnvd.setName(entry.selectFirst("name").text()) ;
            cnnvd.setVulnId(entry.selectFirst("vuln-id").text());
            cnnvd.setPublished(entry.selectFirst("published").text());
            cnnvd.setSeverity(entry.selectFirst("severity").text());
            cnnvd.setVulnDescript(entry.selectFirst("vuln-descript").text());
            cnnvd.setCveId(entry.selectFirst("cve-id").text());
            cnnvd.setvulnSolution(entry.selectFirst("vuln-solution").text());
            cnnvd.setmodified(entry.selectFirst("modified").text());
            cnnvd.setvulntype(entry.selectFirst("vuln-type").text());
            cnnvd.setsource(entry.selectFirst("source").text());
            LOGGER.info(cnnvd.toString());
            UploadCnvd.Upload(cnnvd);
            }

        } catch (Exception e) {
        LOGGER.info("unable to load file", e);
        }
    }

}
