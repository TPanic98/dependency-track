import json
from datetime import datetime
import os
import time
import argparse
import sys
import requests
from reportlab.pdfbase import pdfmetrics   # 注册字体
from reportlab.pdfbase.ttfonts import TTFont # 字体类
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, SimpleDocTemplate, Paragraph, Image  # 报告内容相关类
from reportlab.lib.pagesizes import letter  # 页面的标志尺寸(8.5*inch, 11*inch)
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.styles import getSampleStyleSheet  # 文本样式
from reportlab.lib import colors  # 颜色模块
from reportlab.graphics.charts.barcharts import VerticalBarChart  # 图表类
from reportlab.graphics.charts.legends import Legend  # 图例类
from reportlab.graphics.shapes import Drawing  # 绘图工具
from reportlab.lib.units import cm  # 单位：cm  
import json
import os
import PyPDF2
from datetime import datetime

pathname = 'D:\\vscodews\\python\\dependency-check\\bin\\'
output_path = 'D:\\vscodews\\python\\dependency-check\\bin\\out.json' 
font_path = 'simsun.ttc'
# 注册字体(提前准备好字体文件, 如果同一个文件需要多种字体可以注册多个)
pdfmetrics.registerFont(TTFont('SimSun', font_path))


# 页眉页脚设置
class FooterCanvas(canvas.Canvas):

    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)
        for page in self.pages:
            self.__dict__.update(page)
            self.draw_canvas(page_count)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_canvas(self, page_count):
        page = " www.hkbea.com.cn                        第%s页 共%s页" % (self._pageNumber, page_count)
        x = 128
        self.saveState()
        self.setStrokeColorRGB(0, 0, 0)
        self.setLineWidth(0.5)
        self.line(66, 78, LETTER[0] - 66, 78)
        self.line(66, letter[1]-55, LETTER[0] - 66, letter[1]-55)
        self.setFont('SimSun', 15)
        self.setFillColor(aColor=colors.red)
        self.drawString(80, letter[1]-50, "东亚银行（中国）有限公司                        BEA·QingZhui")
        self.setFont('SimSun', 10)
        self.setFillColor(aColor=colors.grey)
        self.drawString(80, 65, page)
        self.restoreState()






class Graphs:
    # 绘制开局标题
    @staticmethod
    def draw_big_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 50            # 字体大小
        ct.leading = 50             # 行间距
        ct.alignment = 0   # 居中
        ct.bold = True
        ct.spaceBefore = 200
        ct.spaceAfter = 500
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    
    # 绘制大横杆
    @staticmethod
    def draw_longbar(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 1            # 字体大小
        ct.leading = 2             # 行间距
        ct.alignment = 0            # 不居中
        ct.bold = True
        ct.backColor = colors.silver
        ct.spaceBefore = 13
        ct.spaceAfter = 1
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    # 绘制小横杆
    @staticmethod
    def draw_smallbar(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 1            # 字体大小
        ct.leading = 1             # 行间距
        ct.alignment = 0            # 不居中
        ct.bold = True
        ct.backColor = colors.lightgrey
        ct.spaceBefore = 0
        ct.spaceAfter = 0
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    
    # 绘制标题
    @staticmethod
    def draw_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Heading1']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'      # 字体名
        ct.fontSize = 20            # 字体大小
        ct.leading = 10             # 行间距
        ct.textColor = colors.black     # 字体颜色
        ct.alignment = 0    # 居中
        ct.bold = True
        ct.spaceBefore = 10
        ct.spaceAfter = 0

        # ct.backColor = colors.green
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
      
  # 绘制小标题
    @staticmethod
    def draw_little_title(title: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 拿到标题样式
        ct = style['Normal']
        # 单独设置样式相关属性
        ct.fontName = 'SimSun'  # 字体名
        ct.fontSize = 15  # 字体大小
        ct.leading = 15  # 行间距
        ct.textColor = colors.black  # 字体颜色
        ct.spaceBefore = 10
        ct.spaceAfter = 5
        # ct.backColor = colors.green
        # 创建标题对应的段落，并且返回
        return Paragraph(title, ct)
    

    # 绘制普通段落内容
    @staticmethod
    def draw_text(text: str):
        # 获取所有样式表
        style = getSampleStyleSheet()
        # 获取普通样式
        ct = style['Normal']
        ct.fontName = 'SimSun'
        ct.fontSize = 12
        ct.wordWrap = 'CJK'     # 设置自动换行
        ct.alignment = 0        # 左对齐
        ct.firstLineIndent = 32     # 第一行开头空格
        ct.leading = 25
        return Paragraph(text, ct)

    # 绘制表格
    @staticmethod
    def draw_table(*args):
        # 1列宽度
        # col_width = 200
        style = [
            ('FONTNAME', (0, 0), (-1, -1), 'SimSun'),  # 字体
            ('FONTSIZE', (0, 0), (-1, 0), 13),  # 第一行的字体大小
            ('FONTSIZE', (0, 1), (-1, -1), 10),  # 第二行到最后一行的字体大小
            ('BACKGROUND', (0, 0), (-1, 0), '#d5dae6'),  # 设置第一行背景颜色
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # 第一行水平居中
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),  # 第二行到最后一行左右左对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 所有表格上下居中对齐
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.darkslategray),  # 设置表格内文字颜色
            ('GRID', (0, 0), (-1, -1), 0.5, colors.grey),  # 设置表格框线为grey色，线宽为0.5
            ('SPAN', (0, 0), (1, 0)),  # 合并第一列二三行

        ]
        table = Table(args, colWidths=[100,200], style=style,hAlign='LEFT')
        return table
    # 绘制表格
    @staticmethod
    def draw_table2(*args):
        # 1列宽度
        # col_width = 200
        
        style = [
            ('FONTNAME', (0, 0), (-1, -1), 'SimSun'),  # 字体
            ('FONTSIZE', (0, 0), (-1, 0), 13),  # 第一行的字体大小
            ('FONTSIZE', (0, 1), (-1, -1), 10),  # 第二行到最后一行的字体大小
            ('BACKGROUND', (0, 0), (-1, 0), '#d5dae6'),  # 设置第一行背景颜色
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # 第一行水平居中
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),  # 第二行到最后一行左右左对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 所有表格上下居中对齐
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.darkslategray),  # 设置表格内文字颜色
            ('GRID', (0, 0), (-1, -1), 0.5, colors.grey),  # 设置表格框线为grey色，线宽为0.5
            ('SPAN', (0, 0), (-1, 0)),  # 合并第一列二三行            
        ]
        table = Table(args, colWidths=[150,200,70,50], style=style,hAlign='LEFT',)
        return table
    # 创建图表
    @staticmethod
    def draw_bar(bar_data: list, ax: list, items: list):
        drawing = Drawing(500, 250)
        bc = VerticalBarChart()
        bc.x = 45       # 整个图表的x坐标
        bc.y = 45      # 整个图表的y坐标
        bc.height = 200     # 图表的高度
        bc.width = 350      # 图表的宽度
        bc.data = bar_data
        bc.strokeColor = colors.black       # 顶部和右边轴线的颜色
        bc.valueAxis.valueMin = 5000           # 设置y坐标的最小值
        bc.valueAxis.valueMax = 26000         # 设置y坐标的最大值
        bc.valueAxis.valueStep = 2000         # 设置y坐标的步长
        bc.categoryAxis.labels.dx = 2
        bc.categoryAxis.labels.dy = -8
        bc.categoryAxis.labels.angle = 20
        bc.categoryAxis.categoryNames = ax

        # 图示
        leg = Legend()
        leg.fontName = 'SimSun'
        leg.alignment = 'right'
        leg.boxAnchor = 'ne'
        leg.x = 475         # 图例的x坐标
        leg.y = 240
        leg.dxTextSpace = 10
        leg.columnMaximum = 3
        leg.colorNamePairs = items
        drawing.add(leg)
        drawing.add(bc)
        return drawing

    # 绘制图片
    @staticmethod
    def draw_img(path):
        img = Image(path)       # 读取指定路径下的图片
        img.drawWidth = 5*cm        # 设置图片的宽度
        img.drawHeight = 8*cm       # 设置图片的高度
        return img



if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog = 'jar_checker',description = 'Upload a jar to Dependency check')
    parser.add_argument('-j', '--jar', help='Path to the jar file to upload') 
    # parser.add_argument('-u', '--url', help='Dependency Track base URL like http://localhost:8080')
    # parser.add_argument('-k', '--key', help='Dependency Track API Key')
    args = parser.parse_args()

        
    if (args.jar is None ):
        parser.print_help()
        sys.exit(1)
    try:
        data = open(args.jar, 'rb').read()
    except:
        print ('Error: could not open file '+args.sbom+' for reading')
        sys.exit(1)

    
    jar_pathname = args.jar
    
    cmd = pathname + 'dependency-check.bat '+'-format JSON -noupdate '+'-out '+output_path+' -scan '+jar_pathname 

    res = os.popen(cmd)
    out_str = res.read()

    # print('***************out_str**************',out_str)
    fo = open(output_path, "r",encoding='utf-8')
    json_dict = json.load(fo)
    dependencies = json_dict['dependencies']

    # 创建内容对应的空列表
    content = list()
    content2 = list()
    style1 = getSampleStyleSheet()
    style2 = style1['Normal']
    style2.fontSize = 1

    content2.append(Image('D:\\vscodews\\python\\bea2.png',width=21*cm,height=27*cm,hAlign='LEFT',))

    # content.append(Paragraph('1',style=style2))
    # content.append(Graphs.draw_big_title('青追漏洞管理平台\n漏洞报告'))
    # 添加标题e
    content.append(Graphs.draw_title(str('项目'+json_dict['projectInfo']['name']+'的扫描结果')))
    content.append(Graphs.draw_longbar('1'))
    # 添加小标题
    content.append(Graphs.draw_little_title('项目信息'))
    content.append(Graphs.draw_smallbar('1'))
    content.append(Graphs.draw_title(''))

    print(os.path.basename(args.jar))
    # 添加表格
    data1 = [
        ('项目信息',),
        ('项目名称', os.path.basename(args.jar)),
        ('扫描时间',json_dict['scanInfo']['dataSource'][0]['timestamp']),
        ('扫描引擎版本',json_dict['scanInfo']['engineVersion'])  
    ]
    content.append(Graphs.draw_table(*data1))
    print (data1)
    # 添加小标题
    content.append(Graphs.draw_little_title('漏洞信息'))
    content.append(Graphs.draw_smallbar('1'))
    content.append(Graphs.draw_title(''))

    # 添加表格
    CRITICALr = 0
    HIGHr = 0
    MEDIUMr = 0
    LOWr = 0
    data3 = [
        ('具体信息前往青追平台查看：',),
        ('组件名称', '包路径','最高严重程度','漏洞数量'),       
    ]

    for dp in dependencies:
        if dp.get('vulnerabilities') != None:
            fileName = dp['fileName'].split(':')[-1]
            filePackage = 'None'
            if dp.get('packages') != None:
                filePackage = dp['packages'][0]['id'].split(':')[-1]
            cveCount = 0
            highestSeverity = 'Safe'        
            for vul in dp['vulnerabilities']:
                cveCount = cveCount + 1
                if vul['severity'] == 'CRITICAL' :
                    CRITICALr = CRITICALr + 1
                    if highestSeverity != 'CRITICAL':
                        highestSeverity = 'CRITICAL'
                    continue
                if vul['severity'] == 'HIGH'  :
                    HIGHr = HIGHr + 1
                    if highestSeverity != 'CRITICAL' and highestSeverity != 'HIGH':
                        highestSeverity = 'HIGH'
                    continue
                if vul['severity'] == 'MEDIUM':
                    MEDIUMr = MEDIUMr + 1
                    if highestSeverity != 'CRITICAL' and highestSeverity != 'HIGH'and highestSeverity != 'MEDIUM':
                        highestSeverity = 'MEDIUM'
                    continue
                if vul['severity'] == 'LOW':
                    LOWr = LOWr + 1
                    if highestSeverity != 'CRITICAL' and highestSeverity != 'HIGH'and highestSeverity != 'MEDIUM'and highestSeverity != 'LOW':                    
                        highestSeverity = 'LOW'

            data3.append((
                Paragraph(fileName),
                Paragraph(filePackage),
                Paragraph(highestSeverity),
                cveCount
        )) 
    data2 = [
        ('漏洞统计',),
        ('严重', CRITICALr),
        ('高危',HIGHr),
        ('中危',MEDIUMr),
        ('低危险',LOWr)
    ]
    

    
    content.append(Graphs.draw_table(*data2))

    content.append(Graphs.draw_little_title('详细信息'))
    # 添加表格
    content.append(Graphs.draw_table2(*data3))


    # 生成pdf文件

    doc2 = SimpleDocTemplate('report2.pdf', pagesize=[21*cm,27.5*cm],leftMargin = 0,rightMargin = 0,topMargin = 0,bottomMargin = 0)
    doc2.build(content2)


    doc = SimpleDocTemplate('report_jar.pdf', pagesize=letter)
    doc.build(content,canvasmaker=FooterCanvas)


    filenames = ['report2.pdf','report_jar.pdf']
    merger = PyPDF2.PdfMerger()
    for file in filenames:
        merger.append(PyPDF2.PdfReader(file))
    merger.write('jar包扫描结果.pdf')
    os.remove('report_jar.pdf')
    os.remove('report2.pdf')
    fo.close

    os.system("jar包扫描结果.pdf")