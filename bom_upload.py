import argparse
import sys
import requests
# 使用事例：python bom_upload.py -s sbom.json -n cmdUploadProject -v 1.0.0 
parser = argparse.ArgumentParser(prog = 'dt_sbom_uploader',description = 'Upload a sbom to Dependency Track')
parser.add_argument('-s', '--sbom', help='Path to the SBOM file to upload') 
parser.add_argument('-n', '--name', help='project name') 
parser.add_argument('-v', '--version', help='project version')
# parser.add_argument('-k', '--key', help='API Key')
args = parser.parse_args()
print(args.name)

if (args.sbom is None or args.name is None or args.version is None ):
    parser.print_help()
    sys.exit(1)

try:
    data = open(args.sbom, 'rb').read()
except:
    print ('Error: could not open file '+args.sbom+' for reading')
    sys.exit(1) 

# Build the POST request
post_data = {}
post_data['projectName'] = args.name
post_data['projectVersion'] = args.version
post_data['autoCreate'] = 'true'
bomFile = {'bom': data}
key = 'alpine_dbA7XOZBIth4FJpNaxByouTR69H8TRQl '
api_key_header = {'X-Api-Key': key}

aurl = 'http://127.0.0.1:8080'

r = requests.post(url=aurl+'/api/v1/bom', files=bomFile, data=post_data, headers=api_key_header)

print (r)
print (r.text)

if r.status_code == 200:
    print('上传成功')
else:
    print('上传失败')
